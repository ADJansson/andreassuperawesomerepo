cmake_minimum_required(VERSION 3.0)

project (SuperAS VERSION 1.0)

set (HDRS
        functions.h)

set ( SRCS
    main.cpp)

add_executable( ${CMAKE_PROJECT_NAME} ${HDRS} ${SRCS})
